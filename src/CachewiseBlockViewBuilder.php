<?php

namespace Drupal\cache_tools;

use Drupal\block\BlockViewBuilder;
use Drupal\cache_tools\Service\CacheSanitizer;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Theme\Registry;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides cache-wise block view builder.
 */
class CachewiseBlockViewBuilder extends BlockViewBuilder {

  /**
   * The cache sanitizer.
   *
   * @var \Drupal\cache_tools\Service\CacheSanitizer
   */
  protected $cacheSanitizer;

  /**
   * Constructs a new CachewiseBlockViewBuilder.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The module handler.
   * @param \Drupal\Core\Theme\Registry $theme_registry
   *   The theme registry.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity display repository.
   * @param \Drupal\cache_tools\Service\CacheSanitizer $cacheSanitizer
   *   The cache sanitizer.
   */

  public function __construct(EntityTypeInterface $entity_type, EntityRepositoryInterface $entity_repository, LanguageManagerInterface $language_manager, Registry $theme_registry, EntityDisplayRepositoryInterface $entity_display_repository, CacheSanitizer $cacheSanitizer) {
    parent::__construct($entity_type, $entity_repository, $language_manager, $theme_registry, $entity_display_repository);
    $this->cacheSanitizer = $cacheSanitizer;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity.repository'),
      $container->get('language_manager'),
      $container->get('theme.registry'),
      $container->get('entity_display.repository'),
      $container->get('cache_tools.cache.sanitizer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewMultiple(array $entities = [], $view_mode = 'full', $langcode = NULL) {
    $build = parent::viewMultiple($entities, $view_mode, $langcode);
    foreach ($entities as $block) {
      $this->cacheSanitizer->sanitize($block, $build[$block->id()]);
    }
    return $build;
  }

  /**
   * Sanitize the build also during prerendering.
   *
   * Helps to prevent views block to place undesired cacheable metadata.
   *
   * {@inheritdoc}
   */
  public static function preRender($build) {
    static $cacheSanitizer;
    if (!isset($cacheSanitizer)) {
      /** @var \Drupal\cache_tools\Service\CacheSanitizer $cacheSanitizer */
      $cacheSanitizer = \Drupal::service('cache_tools.cache.sanitizer');
    }
    $block = $build['#block'];
    $build = parent::preRender($build);
    $cacheSanitizer->sanitize($block, $build);
    return $build;
  }

}
